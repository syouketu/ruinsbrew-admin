package com.dongjiu.gen.dao;

import com.dongjiu.common.persistence.CrudDao;
import com.dongjiu.common.persistence.annotation.MyBatisDao;
import com.dongjiu.gen.entity.GenTable;
import com.dongjiu.gen.entity.GenTableColumn;

import java.util.List;

/**
 * GenDataBaseDictDao
 * 业务表字段DAO接口
 *
 * @author xiaojie
 * @date 2016/10/20
 */
@MyBatisDao
public interface GenDataBaseDictDao extends CrudDao<GenTableColumn> {

    /**
     * 查询表列表
     *
     * @param genTable
     * @return
     */
    List<GenTable> findTableList(GenTable genTable);

    /**
     * 获取数据表字段
     *
     * @param genTable
     * @return
     */
    List<GenTableColumn> findTableColumnList(GenTable genTable);

    /**
     * 获取数据表主键
     *
     * @param genTable
     * @return
     */
    List<String> findTablePK(GenTable genTable);

}
