package com.dongjiu.gen.dao;

import com.dongjiu.common.persistence.CrudDao;
import com.dongjiu.common.persistence.annotation.MyBatisDao;
import com.dongjiu.gen.entity.GenScheme;

/**
 * GenSchemeDao
 * 生成方案DAO接口
 *
 * @author xiaojie
 * @date 2016/10/20
 */
@MyBatisDao
public interface GenSchemeDao extends CrudDao<GenScheme> {

}
