package com.dongjiu.gen.dao;

import com.dongjiu.common.persistence.CrudDao;
import com.dongjiu.common.persistence.annotation.MyBatisDao;
import com.dongjiu.gen.entity.GenTableColumn;

/**
 * GenTableColumnDao
 * 业务表字段DAO接口
 *
 * @author xiaojie
 * @date 2016/10/20
 */
@MyBatisDao
public interface GenTableColumnDao extends CrudDao<GenTableColumn> {

    public void deleteByGenTableId(String genTableId);
}
