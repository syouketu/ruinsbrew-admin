package com.dongjiu.gen.dao;

import com.dongjiu.common.persistence.CrudDao;
import com.dongjiu.common.persistence.annotation.MyBatisDao;
import com.dongjiu.gen.entity.GenTable;

/**
 * GenTableDao
 * 业务表DAO接口
 *
 * @author xiaojie
 * @date 2016/10/20
 */
@MyBatisDao
public interface GenTableDao extends CrudDao<GenTable> {

}
