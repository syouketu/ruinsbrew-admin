package com.dongjiu.sys.listener;

import org.springframework.web.context.WebApplicationContext;

import javax.servlet.ServletContext;

//import com.dongjiu.modules.sys.service.SystemService;

/**
* WebContextListener
* @Author xiaojie
* @Date 2016/10/20
*/
public class WebContextListener extends org.springframework.web.context.ContextLoaderListener {

    @Override
    public WebApplicationContext initWebApplicationContext(ServletContext servletContext) {
//        if (!SystemService.printKeyLoadMessage()){
//            return null;
//        }
        return super.initWebApplicationContext(servletContext);
    }
}
