package com.dongjiu.sys.dao;

import com.dongjiu.common.persistence.CrudDao;
import com.dongjiu.common.persistence.annotation.MyBatisDao;
import com.dongjiu.sys.entity.Menu;

import java.util.List;

/**
 * MenuDao
 * 菜单DAO接口
 *
 * @author xiaojie
 * @date 2016/10/20
 */
@MyBatisDao
public interface MenuDao extends CrudDao<Menu> {

    public List<Menu> findByParentIdsLike(Menu menu);

    public List<Menu> findByUserId(Menu menu);

    public int updateParentIds(Menu menu);

    public int updateSort(Menu menu);

}
