package com.dongjiu.sys.dao;

import com.dongjiu.common.persistence.TreeDao;
import com.dongjiu.common.persistence.annotation.MyBatisDao;
import com.dongjiu.sys.entity.Office;

/**
 * OfficeDao
 * 机构DAO接口
 *
 * @author xiaojie
 * @date 2016/10/20
 */
@MyBatisDao
public interface OfficeDao extends TreeDao<Office> {

}
