package com.dongjiu.sys.dao;

import com.dongjiu.common.persistence.CrudDao;
import com.dongjiu.common.persistence.annotation.MyBatisDao;
import com.dongjiu.sys.entity.Log;

/**
 * LogDao
 * 日志DAO接口
 *
 * @author xiaojie
 * @date 2016/10/20
 */
@MyBatisDao
public interface LogDao extends CrudDao<Log> {

}
