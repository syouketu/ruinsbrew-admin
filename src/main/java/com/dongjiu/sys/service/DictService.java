package com.dongjiu.sys.service;

import com.dongjiu.common.service.CrudService;
import com.dongjiu.common.utils.CacheUtils;
import com.dongjiu.sys.dao.DictDao;
import com.dongjiu.sys.entity.Dict;
import com.dongjiu.sys.utils.DictUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * DictService
 * 字典Service
 *
 * @author xiaojie
 * @date 2016/10/20
 */
@Service
@Transactional(readOnly = true)
public class DictService extends CrudService<DictDao, Dict> {

    /**
     * 查询字段类型列表
     *
     * @return
     */
    public List<String> findTypeList() {
        return dao.findTypeList(new Dict());
    }

    @Transactional(readOnly = false)
    public void save(Dict dict) {
        super.save(dict);
        CacheUtils.remove(DictUtils.CACHE_DICT_MAP);
    }

    @Transactional(readOnly = false)
    public void delete(Dict dict) {
        super.delete(dict);
        CacheUtils.remove(DictUtils.CACHE_DICT_MAP);
    }

}
