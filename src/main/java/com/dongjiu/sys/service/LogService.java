package com.dongjiu.sys.service;

import com.dongjiu.common.persistence.Page;
import com.dongjiu.common.service.CrudService;
import com.dongjiu.common.utils.DateUtils;
import com.dongjiu.sys.dao.LogDao;
import com.dongjiu.sys.entity.Log;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * LogService
 * 日志Service
 *
 * @author xiaojie
 * @date 2016/10/20
 */
@Service
@Transactional(readOnly = true)
public class LogService extends CrudService<LogDao, Log> {

    public Page<Log> findPage(Page<Log> page, Log log) {

        // 设置默认时间范围，默认当前月
        if (log.getBeginDate() == null) {
            log.setBeginDate(DateUtils.setDays(DateUtils.parseDate(DateUtils.getDate()), 1));
        }
        if (log.getEndDate() == null) {
            log.setEndDate(DateUtils.addMonths(log.getBeginDate(), 1));
        }

        return super.findPage(page, log);

    }

}
