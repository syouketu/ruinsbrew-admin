/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.dongjiu.common.utils.excel.fieldtype;

import com.dongjiu.common.utils.StringUtils;
import com.dongjiu.sys.entity.Office;
import com.dongjiu.sys.utils.UserUtils;

/**
 * OfficeType
 * 字段类型转换
 *
 * @author xiaojie
 * @date 2016/10/21
 */
public class OfficeType {

    /**
     * 获取对象值（导入）
     */
    public static Object getValue(String val) {
        for (Office e : UserUtils.getOfficeList()) {
            if (StringUtils.trimToEmpty(val).equals(e.getName())) {
                return e;
            }
        }
        return null;
    }

    /**
     * 设置对象值（导出）
     */
    public static String setValue(Object val) {
        if (val != null && ((Office) val).getName() != null) {
            return ((Office) val).getName();
        }
        return "";
    }
}
