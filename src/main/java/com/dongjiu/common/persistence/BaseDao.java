/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.dongjiu.common.persistence;

/**
 * BaseDao
 * DAO支持类实现
 *
 * @author xiaojie
 * @date 2016/10/20
 */
public interface BaseDao {

}