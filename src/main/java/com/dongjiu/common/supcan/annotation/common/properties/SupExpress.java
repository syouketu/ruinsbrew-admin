/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.dongjiu.common.supcan.annotation.common.properties;

import java.lang.annotation.*;

/**
 * SupExpress
 * Express注解
 *
 * @author xiaojie
 * @date 2016/10/21
 */
@Target({ElementType.ANNOTATION_TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Inherited
public @interface SupExpress {

    /**
     * 是否自动按列的引用关系优化计算顺序  默认值true
     */
    String isOpt() default "";

    /**
     * 文本
     */
    String text() default "";

}
