package com.dongjiu.common.supcan.annotation.common.properties;

import java.lang.annotation.*;

/**
 * SupBackground
 * Background注解
 *
 * @author xiaojie
 * @date 2016/10/21
 */
@Target({ElementType.ANNOTATION_TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Inherited
public @interface SupBackground {

    /**
     * 背景颜色
     *
     * @return
     */
    String bgColor() default "";

}
