package com.dongjiu.common.supcan.freeform;

import com.dongjiu.common.supcan.common.Common;
import com.dongjiu.common.supcan.common.properties.Properties;
import com.thoughtworks.xstream.annotations.XStreamAlias;

/**
 * FreeForm
 *
 * @author xiaojie
 * @date 2016/10/21
 */
@XStreamAlias("FreeForm")
public class FreeForm extends Common {

    public FreeForm() {
        super();
    }

    public FreeForm(Properties properties) {
        this();
        this.properties = properties;
    }

}
