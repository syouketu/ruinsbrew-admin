<%@ tag language="java" pageEncoding="UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp" %>
<%@ attribute name="name" type="java.lang.String" required="true" description="验证码输入框名称" %>
<%@ attribute name="inputCssStyle" type="java.lang.String" required="false" description="验证框样式" %>
<%@ attribute name="imageCssStyle" type="java.lang.String" required="false" description="验证码图片样式" %>
<%@ attribute name="buttonCssStyle" type="java.lang.String" required="false" description="看不清按钮样式" %>
<div class="form-group">
    <div class="input-group input-group-md">
           <span class="input-group-addon""><i class="glyphicon glyphicon-barcode"
                                                                 aria-hidden="true"></i></span>
        <input type="text" id="${name}" name="${name}" class="form-control"
               style="font-weight:bold;width:100px;${inputCssStyle}" placeholder="验证码"/>
        <img src="${pageContext.request.contextPath}/servlet/validateCodeServlet"
             onclick="$('.${name}Refresh').click();"
             class="${name}" style="${imageCssStyle}"/>
        <a href="javascript:" onclick="$('.validateCode').attr('src','/servlet/validateCodeServlet?'+new Date().getTime());" class="mid validateCodeRefresh" style="">看不清</a>
    </div>

</div>