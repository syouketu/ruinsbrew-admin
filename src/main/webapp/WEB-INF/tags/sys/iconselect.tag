<%@ tag language="java" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<%@ attribute name="id" type="java.lang.String" required="true" description="编号"%>
<%@ attribute name="name" type="java.lang.String" required="true" description="输入框名称"%>
<%@ attribute name="value" type="java.lang.String" required="true" description="输入框值"%>
<div class="col-sm-1">
    <i id="${id}Icon" class="${not empty value?value:' hide'}"></i>&nbsp;<label id="${id}IconLabel">${not empty value?value:'无'}</label>&nbsp;
    <input id="${id}" name="${name}" type="hidden" value="${value}"/>
</div>
<div class="col-sm-9">
    <button type="button" id="${id}Button" class="btn btn-primary" data-btn-type="selectIcon">
        <i class="fa fa-hand-pointer-o">&nbsp;选择图标</i>
    </button>
    </div>

<script type="text/javascript">
	$("#${id}Button").click(function(){

//		console.log($(top.document).height()-180);
		//iframe窗
		layer.open({
			type: 2,
			title: "选择图标",
			closeBtn: 1, //不显示关闭按钮
			shadeClose: true,
			shade: 0.8,
			area: ['900px', ($(top.document).height())+'px'],
//			offset: 'rb', //右下角弹出
			anim: 2,
			content: "${ctx}/tag/iconselect?value="+$('#${id}').val(),
			btn: ['确定', '关闭'],
			yes: function(index, layero){ //或者使用btn1
				var iframeWin = window[layero.find('iframe')[0]['name']]; //得到iframe页的窗口对象，执行iframe页的方法：iframeWin.method();
				var icon = iframeWin.$("#icon").val();
				$("#${id}Icon").attr("class", icon);
				$("#${id}IconLabel").text(icon);
				$("#${id}").val(icon);
                layer.closeAll();
				return false;
			},
			btn2: function(index, layero){

				$("#${id}Icon").attr("class", "icon- hide");
				$("#${id}IconLabel").text("无");
				$("#${id}").val("");
				layer.closeAll();
			}
		});
	});
</script>
