<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp" %>
<!DOCTYPE html>
<html>
<head>
    <%@ include file="/WEB-INF/views/include/common.jsp" %>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<!-- Site wrapper -->
<div class="wrapper">

    <%@ include file="/WEB-INF/views/include/header.jsp" %>

    <!-- =============================================== -->

    <%@ include file="/WEB-INF/views/include/menu.jsp" %>

    <!-- =============================================== -->

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper" id="mainDiv">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                个人信息
            </h1>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="box">
                <form:form id="inputForm" modelAttribute="user" action="${ctx}/sys/user/info" method="post"
                           class="form-horizontal">
                    <sys:message content="${message}"/>
                    <div class="box-body">
                        <div class="form-group">
                            <label class="control-label col-sm-2">头像:</label>
                            <div class="controls col-sm-3">
                                <form:hidden id="nameImage" path="photo" htmlEscape="false" maxlength="255"
                                             class="input-xlarge"/>
                                <sys:ckfinder input="nameImage" type="images" uploadPath="/photo" selectMultiple="false"
                                              maxWidth="100" maxHeight="100"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-2">归属公司:</label>
                            <div class="controls col-sm-3">
                                <input type="text" class="form-control" disabled value="${user.company.name}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-2">归属部门:</label>
                            <div class="controls col-sm-3">
                                <input type="text" class="form-control" disabled value="${user.office.name}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-2">姓名:</label>
                            <div class="controls col-sm-3">
                                <form:input path="name" htmlEscape="false" maxlength="50" class="form-control"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-2">邮箱:</label>
                            <div class="controls col-sm-3">
                                <form:input path="email" htmlEscape="false" maxlength="50" class="form-control"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-2">电话:</label>
                            <div class="controls col-sm-3">
                                <form:input path="phone" htmlEscape="false" maxlength="50"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-2">手机:</label>
                            <div class="controls col-sm-3">
                                <form:input path="mobile" htmlEscape="false" maxlength="50"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-2">备注:</label>
                            <div class="controls col-sm-3">
                                <form:textarea path="remarks" htmlEscape="false" rows="3" maxlength="200"
                                               class="form-control"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-2">用户类型:</label>
                            <div class="controls col-sm-3">
                                <input type="text" class="form-control" disabled
                                       value="${fns:getDictLabel(user.userType, 'sys_user_type', '无')}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-2">用户角色:</label>
                            <div class="controls col-sm-3">
                                <input type="text" class="form-control" disabled value="${user.roleNames}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-2">上次登录:</label>
                            <div class="controls col-sm-3">
                                <input type="text" class="form-control" disabled
                                       value="IP: ${user.oldLoginIp}&nbsp;&nbsp;时间：<fmt:formatDate value="${user.oldLoginDate}" type="both" dateStyle="full"/>">
                            </div>
                        </div>
                    </div>
                    <div class="box-footer ">
                        <div class="col-sm-offset-2 col-sm-10">
                            <input id="btnSubmit" class="btn btn-primary" type="submit" value="保 存"/>
                        </div>
                    </div>
                </form:form>
            </div>
        </section>
    </div>
    <!-- /.content-wrapper -->


</div>
<!-- ./wrapper -->
<%@ include file="/WEB-INF/views/include/footer.jsp" %>
</body>
</html>
