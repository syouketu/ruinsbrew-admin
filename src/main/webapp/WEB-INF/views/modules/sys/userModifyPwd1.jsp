<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp" %>
<html>
<head>
    <title>修改密码</title>
    <meta name="decorator" content="dj-default"/>
    <script type="text/javascript">
        /*		$(document).ready(function() {
         $("#oldPassword").focus();
         $("#inputForm").validate({
         rules: {
         },
         messages: {
         confirmNewPassword: {equalTo: "输入与上面相同的密码"}
         },
         submitHandler: function(form){
         loading('正在提交，请稍等...');
         form.submit();
         },
         errorContainer: "#messageBox",
         errorPlacement: function(error, element) {
         $("#messageBox").text("输入有误，请先更正。");
         if (element.is(":checkbox")||element.is(":radio")||element.parent().is(".input-append")){
         error.appendTo(element.parent().parent());
         } else {
         error.insertAfter(element);
         }
         }
         });
         });*/
    </script>
</head>
<body>
<!-- Main content -->
<div class="wrapper">
    <div class="dj-content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                修改密码
            </h1>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="box">
                <form:form id="inputForm" modelAttribute="user" action="${ctx}/sys/user/modifyPwd" method="post"
                           class="form-horizontal">
                    <form:hidden path="id"/>
                    <sys:message content="${message}"/>
                    <div class="box-body">
                        <div class="form-group">
                            <label class="control-label col-sm-2">旧密码:</label>
                            <div class="controls col-sm-3">
                                <input id="oldPassword" name="oldPassword" type="password" value="" maxlength="50"
                                       minlength="3" class="required"/>
                                <span class="help-inline"><font color="red">*</font> </span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-2">新密码:</label>
                            <div class="controls col-sm-3">
                                <input id="newPassword" name="newPassword" type="password" value="" maxlength="50"
                                       minlength="3" class="required"/>
                                <span class="help-inline"><font color="red">*</font> </span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-2">确认新密码:</label>
                            <div class="controls col-sm-3">
                                <input id="confirmNewPassword" name="confirmNewPassword" type="password" value=""
                                       maxlength="50" minlength="3" class="required" equalTo="#newPassword"/>
                                <span class="help-inline"><font color="red">*</font> </span>
                            </div>
                        </div>
                    </div>
                    <div class="box-footer ">
                        <div class="col-sm-offset-2 col-sm-10">
                            <input id="btnSubmit" class="btn btn-primary" type="submit" value="保 存"/>
                        </div>
                    </div>
                </form:form>
            </div>
        </section>
    </div>
</div>
</body>
</html>