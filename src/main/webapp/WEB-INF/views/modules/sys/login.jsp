<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp" %>
<!DOCTYPE html>
<html>
<head>
    <title>${fns:getConfig('productName')} 登录</title>
    <meta name="decorator" content="dj-blank"/>
</head>

<body class="hold-transition login-page">
<div class="login-box">
    <div class="header">
        <div id="messageBox" class="alert alert-error ${empty message ? 'hide' : ''}">
            <button data-dismiss="alert" class="close">×</button>
            <label id="loginError" class="error">${message}</label>
        </div>
    </div>
    <div class="login-logo">
        <a href="#">${fns:getConfig('productName')}</a>
    </div>
    <!-- /.login-logo -->
    <div class="login-box-body">
        <p class="login-box-msg"></p>

        <form id="loginForm" method="post">
            <div class="form-group">
                <div class="input-group input-group-md">
                    <span class="input-group-addon"><i class="glyphicon glyphicon-user"
                                                       aria-hidden="true"></i></span>
                    <input type="text" class="form-control" placeholder="用户名" name="username">
                </div>
            </div>
            <div class="form-group">
                <div class="input-group input-group-md">
                    <span class="input-group-addon"><i class="glyphicon glyphicon-lock"
                                                       aria-hidden="true"></i></span>
                    <input type="password" class="form-control" placeholder="密码" name="password">
                </div>
            </div>
            <c:if test="${isValidateCodeLogin}">
                <sys:validateCode name="validateCode" inputCssStyle="margin-bottom:0;"/>
            </c:if>
            <div class="row">
                <div class="col-xs-8">
                    <div class="checkbox icheck">
                        <label>
                            <input type="checkbox"> 记住我
                        </label>
                    </div>
                </div>
                <!-- /.col -->
                <div class="col-xs-4">
                    <button type="submit" class="btn btn-primary btn-block btn-flat">登录</button>
                </div>
                <!-- /.col -->
            </div>
        </form>

        <a href="#">忘记密码</a><br>
        <a href="register.html" class="text-center">注册会员</a>

    </div>
    <!-- /.login-box-body -->
</div>
<script type="text/javascript">
    $(document).ready(function () {
        $('input').iCheck({
            checkboxClass: 'icheckbox_square-blue',
            radioClass: 'iradio_square-blue',
            increaseArea: '20%' // optional
        });

        $("#loginForm").bootstrapValidator({
            framework: 'bootstrap',
            message: '请输入有效值',
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                username: {
                    validators: {
                        notEmpty: {
                            message: '请输入用户名'
                        }
                    }
                },
                password: {
                    validators: {
                        notEmpty: {
                            message: '请输入密码'
                        },
                        different: {
                            field: 'username',
                            message: '密码不能和用户名相同'
                        },
                    }
                },
                validateCode: {
                    verbose: false,
                    validators: {
                        notEmpty: {
                            message: '请输入验证码'
                        },
                        stringLength: {
                            min: 4,
                            max: 4,
                            message: "请输入4位验证码"
                        },
                        threshold: 4,//4个字符才发送验证请求
                        remote: {
                            url: "${pageContext.request.contextPath}/servlet/validateCodeServlet",
                            message: '验证码不符',
//                            delay: 2000,
                            type: "POST",
                        }
                    }
                }
            }
        }).on('success.form.bv', function (e) {
//            e.preventDefault();
        });
    });
</script>
</body>
</html>
