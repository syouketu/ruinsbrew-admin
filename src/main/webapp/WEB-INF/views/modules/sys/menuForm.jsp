<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp" %>
<!DOCTYPE html>
<html>
<head>
    <%@ include file="/WEB-INF/views/include/common.jsp"%>
    <style>
        label {
            display: inline-block;
            margin-bottom: 0;
        }

        input[type="radio"], input[type="checkbox"] {
            margin: 7px 2px 2px 10px;
        }
    </style>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<!-- Site wrapper -->
<div class="wrapper">

    <%@ include file="/WEB-INF/views/include/header.jsp"%>

    <!-- =============================================== -->

    <%@ include file="/WEB-INF/views/include/menu.jsp"%>

    <!-- =============================================== -->

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper" id="mainDiv">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                菜单修改
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i>首页</a></li>
                <li><a href="#">系统设置</a></li>
                <li class="active">菜单管理</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="box">

                <form:form id="inputForm" modelAttribute="menu" action="${ctx}/sys/menu/save" method="post"
                           class="form-horizontal">
                    <div class="box-body">
                        <form:hidden path="id"/>
                            <%--<sys:message content="${message}"/>--%>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">上级菜单:</label>
                            <sys:treeselect id="menu" name="parent.id" value="${menu.parent.id}"
                                            labelName="parent.name"
                                            labelValue="${menu.parent.name}"
                                            title="菜单" url="/sys/menu/treeData" extId="${menu.id}"
                                            cssClass="form-control required"/>

                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">名称:</label>
                            <div class="col-sm-2 controls">
                                <form:input path="name" htmlEscape="false" maxlength="50"
                                            class="form-control"/>

                            </div>
                            <div class="col-sm-8">
                                <span class="help-inline"><font color="red">*</font> </span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">链接:</label>
                            <div class="col-sm-2">
                                <form:input path="href" htmlEscape="false" maxlength="2000" class="form-control"/>
                            </div>
                            <div class="col-sm-8">
                                <span class="help-inline">点击菜单跳转的页面</span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">目标:</label>
                            <div class="col-sm-2">
                                <form:input path="target" htmlEscape="false" maxlength="10" class="form-control"/>
                            </div>
                            <div class="col-sm-8">
                                <span class="help-inline">链接地址打开的目标窗口，默认：mainFrame</span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">图标:</label>
                            <div class="col-sm-10 controls">
                                <sys:iconselect id="icon" name="icon" value="${menu.icon}"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">排序:</label>
                            <div class="col-sm-2">
                                <form:input path="sort" htmlEscape="false" maxlength="50"
                                            class="form-control"/>
                            </div>
                            <div class="col-sm-8">
                                <span class="help-inline">排列顺序，升序。</span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">可见:</label>
                            <div class="col-sm-2">
                                <form:radiobuttons path="isShow" items="${fns:getDictList('show_hide')}"
                                                   itemLabel="label"
                                                   itemValue="value" htmlEscape="false" class="required"/>
                            </div>
                            <div class="col-sm-8">
                                <span class="help-inline">该菜单或操作是否显示到系统菜单中</span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">权限标识:</label>
                            <div class="col-sm-2 controls">
                                <form:input path="permission" htmlEscape="false" maxlength="100" class="form-control"/>
                            </div>
                            <div class="col-sm-8">
                                <span class="help-inline">控制器中定义的权限标识，如：@RequiresPermissions("权限标识")</span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">备注:</label>
                            <div class="col-sm-2 controls">
                                <form:textarea path="remarks" htmlEscape="false" rows="3" maxlength="200"
                                               class="form-control"/>
                            </div>
                        </div>
                    </div>
                    <div class="box-footer ">
                        <div class="col-sm-offset-2 col-sm-10">
                            <shiro:hasPermission name="sys:menu:edit"><input id="btnSubmit" class="btn btn-primary"
                                                                             type="submit"
                                                                             value="保 存"/>&nbsp;</shiro:hasPermission>
                            <input id="btnCancel" class="btn" type="button" value="返 回" onclick="history.go(-1)"/></div>
                    </div>
                </form:form>

            </div>
        </section>
    </div>
    <!-- /.content-wrapper -->


</div>
<!-- ./wrapper -->
<%@ include file="/WEB-INF/views/include/footer.jsp"%>
<script type="text/javascript">
    $(function () {
        $("#name").focus();

        /*            $("#inputForm").bootstrapValidator({
         framework: 'bootstrap',
         message: '请输入有效值',
         feedbackIcons: {
         valid: 'glyphicon glyphicon-ok',
         invalid: 'glyphicon glyphicon-remove',
         validating: 'glyphicon glyphicon-refresh'
         },
         fields: {
         username: {
         validators: {
         notEmpty: {
         message: '请输入用户名'
         }
         }
         },
         password: {
         validators: {
         notEmpty: {
         message: '请输入密码'
         }
         }
         }
         }
         })*/

    });
</script>
</body>
</html>
