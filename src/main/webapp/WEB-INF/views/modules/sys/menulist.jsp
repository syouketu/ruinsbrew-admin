<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp" %>
<!DOCTYPE html>
<html>
<head>
    <%@ include file="/WEB-INF/views/include/common.jsp"%>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<!-- Site wrapper -->
<div class="wrapper">

    <%@ include file="/WEB-INF/views/include/header.jsp"%>

    <!-- =============================================== -->

    <%@ include file="/WEB-INF/views/include/menu.jsp"%>

    <!-- =============================================== -->

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper" id="mainDiv">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                菜单树
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i>首页</a></li>
                <li><a href="#">系统设置</a></li>
                <li class="active">菜单管理</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="box">
                <form id="listForm" method="post">
                    <table class="table tree table table-striped table-bordered table-condensed">
                        <thead>
                        <tr>
                            <th>名称</th>
                            <th>链接</th>
                            <th style="text-align:center;">排序</th>
                            <th>可见</th>
                            <th>权限标识</th>
                            <th>操作</th>
                        </thead>
                        <tbody><c:forEach items="${list}" var="menu">
                            <tr id="${menu.id}" pId="${menu.parent.id ne '1'?menu.parent.id:'0'}"
                                class="treegrid-${menu.id} treegrid-parent-${menu.parent.id ne '1'?menu.parent.id:''}">
                                <td nowrap><i class="fa ${not empty menu.icon?menu.icon:''}"></i><a
                                        href="${ctx}/sys/menu/form?id=${menu.id}">${menu.name}</a></td>
                                <td title="${menu.href}">${fns:abbr(menu.href,30)}</td>
                                <td style="text-align:center;">
                                    <shiro:hasPermission name="sys:menu:edit">
                                        <input type="hidden" name="ids" value="${menu.id}"/>
                                        <input name="sorts" type="text" value="${menu.sort}"
                                               style="width:50px;margin:0;padding:0;text-align:center;">
                                    </shiro:hasPermission><shiro:lacksPermission name="sys:menu:edit">
                                    ${menu.sort}
                                </shiro:lacksPermission>
                                </td>
                                <td>${menu.isShow eq '1'?'显示':'隐藏'}</td>
                                <td title="${menu.permission}">${fns:abbr(menu.permission,30)}</td>
                                <td nowrap>
                                    <a href="${ctx}/sys/menu/form?id=${menu.id}">修改</a>
                                    <a href="${ctx}/sys/menu/delete?id=${menu.id}"
                                       onclick="return confirmx('要删除该菜单及所有子菜单项吗？', this.href)">删除</a>
                                    <a href="${ctx}/sys/menu/form?parent.id=${menu.id}">添加下级菜单</a>
                                </td>
                            </tr>
                        </c:forEach></tbody>
                    </table>
                </form>
            </div>
        </section>
    </div>
    <!-- /.content-wrapper -->


</div>
<!-- ./wrapper -->
<%@ include file="/WEB-INF/views/include/footer.jsp"%>
<script type="text/javascript">
    $(document).ready(function () {
        $('.tree').treegrid({});
    });
</script>
</body>
</html>
