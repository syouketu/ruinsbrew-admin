<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp" %>
<!DOCTYPE html>
<html>
<head>
    <%@ include file="/WEB-INF/views/include/common.jsp"%>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<!-- Site wrapper -->
<div class="wrapper">

    <%@ include file="/WEB-INF/views/include/header.jsp"%>

    <!-- =============================================== -->

    <%@ include file="/WEB-INF/views/include/menu.jsp"%>

    <!-- =============================================== -->

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper" id="mainDiv">
    </div>
    <!-- /.content-wrapper -->


</div>
<!-- ./wrapper -->
<%@ include file="/WEB-INF/views/include/footer.jsp"%>
</body>
</html>
