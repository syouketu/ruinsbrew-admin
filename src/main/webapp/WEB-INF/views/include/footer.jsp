<%@ page contentType="text/html;charset=UTF-8" %>
<footer class="main-footer">
    <div class="pull-right hidden-xs">
        <b>Version</b> 1.0.0
    </div>
    <strong>Copyright &copy; 2016 <a href="http://www.dongjiusoft.com">东久科技</a>.</strong> All rights
    reserved.
</footer>
