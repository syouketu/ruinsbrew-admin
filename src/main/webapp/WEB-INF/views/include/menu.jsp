<%@ page contentType="text/html;charset=UTF-8" %>
<!-- Left side column. contains the sidebar -->
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="${adminLte}/img/user2-160x160.jpg" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
                <p>${fns:getUser().name}</p>
                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>
        <!-- search form -->
        <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="搜索...">
              <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
            </div>
        </form>
        <!-- /.search form -->
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu">
            <li class="header">导航</li>

            <c:set var="firstMenu" value="true"/>
            <c:set var="menuList" value="${fns:getMenuList()}"/>
            <c:forEach items="${menuList}" var="menu1" varStatus="idxStatus">
                <c:if test="${menu1.parent.id eq '1'&&menu1.isShow eq '1'}">
                    <li class="treeview">
                        <a href="#">
                            <i class="fa ${menu1.icon}"></i>
                            <span>${menu1.name}</span>
                                <span class="pull-right-container">
                                    <i class="fa fa-angle-left pull-right"></i>
                                </span>
                        </a>
                        <ul class="treeview-menu">
                            <c:forEach items="${menuList}" var="menu2">
                                <c:if test="${menu2.parent.id eq menu1.id&&menu2.isShow eq '1'}">

                                    <c:if test="${not empty menu2.remarks}">
                                        <li><a href="${ctx}${menu2.href}"><i class="fa ${menu2.icon}"></i>${menu2.name}</a></li>
                                    </c:if>
                                    <c:if test="${empty menu2.remarks}">
                                        <li>
                                            <a href="#" class="fa ${menu2.icon}">${menu2.name}</a>
                                            <ul class="treeview-menu">
                                                <c:forEach items="${menuList}" var="menu3">
                                                    <c:if test="${menu3.parent.id eq menu2.id&&menu3.isShow eq '1'}">
                                                        <li><a href="${ctx}${menu3.href}"><i class="fa ${menu3.icon}"></i>${menu3.name}</a></li>
                                                    </c:if>
                                                </c:forEach>
                                            </ul>
                                        </li>
                                    </c:if>
                                </c:if>
                            </c:forEach>
                        </ul>
                    </li>
                </c:if>
            </c:forEach>
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>