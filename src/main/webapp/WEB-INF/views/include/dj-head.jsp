<%@ page contentType="text/html;charset=UTF-8" %>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8"/>
<meta name="renderer" content="webkit"><meta http-equiv="X-UA-Compatible" content="IE=8,IE=9,IE=10" />
<meta name="author" content="http://www.dongjiusoft.com/"/>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">

<!-- Tell the browser to be responsive to screen width -->
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
<!-- Bootstrap 3.3.6 -->
<link rel="stylesheet" href="${adminLte}/bootstrap/css/bootstrap.min.css">
<!-- Font Awesome -->
<link rel="stylesheet" href="${adminLte}/plugins/font-awesome/css/font-awesome.min.css">
<!-- Ionicons -->
<link rel="stylesheet" href="${adminLte}/css/thirdparty/ionicons.min.css">
<!-- JQuery confirm -->
<link rel="stylesheet" href="${adminLte}/plugins/jquery-confirm/jquery-confirm.min.css">
<!-- Theme style -->
<link rel="stylesheet" href="${adminLte}/css/AdminLTE.min.css">
<link rel="stylesheet" href="${adminLte}/css/dongjiu.css">
<!-- iCheck -->
<link rel="stylesheet" href="${adminLte}/plugins/iCheck/square/blue.css">
<link rel="stylesheet" href="${adminLte}/plugins/jquery-treegrid/css/jquery.treegrid.css">
<!-- AdminLTE Skins. Choose a skin from the css/skins
folder instead of downloading all of them to reduce the load. -->
<link rel="stylesheet" href="${adminLte}/css/skins/_all-skins.min.css">
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
<script src="${adminLte}/thirdparty/js/html5shiv.min.js"></script>
<script src="${adminLte}/thirdparty/js/respond.min.js"></script>
<![endif]-->

<!-- jQuery 2.2.3 -->
<script src="${adminLte}/plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="${adminLte}/bootstrap/js/bootstrap.min.js"></script>
<!-- JQuery confirm -->
<script src="${adminLte}/plugins/jquery-confirm/jquery-confirm.min.js"></script>
<!-- iCheck -->
<script src="${adminLte}/plugins/iCheck/icheck.min.js"></script>
<!-- form validate -->
<script src="${adminLte}/plugins/bootstrap-validator/dist/js/bootstrapValidator.js"></script>
<!-- Bootstrp modal -->
<script src="${adminLte}/js/base-modal.js"></script>
<script type="text/javascript" src="${adminLte}/plugins/jquery-treegrid/js/jquery.treegrid.js"></script>
<script type="text/javascript" src="${adminLte}/plugins/jquery-treegrid/js/jquery.treegrid.bootstrap3.js"></script>
<script type="text/javascript" src="${adminLte}/plugins/layer/layer.js"></script>
<script type="text/javascript" src="${adminLte}/js/app.min.js"/>
<script type="text/javascript" src="${adminLte}/js/base-modal.js"></script>
<script type="text/javascript" src="${adminLte}/js/dongjiu.js"></script>
<script type="text/javascript">var ctx = '${ctx}', adminLte = '${adminLte}';</script>